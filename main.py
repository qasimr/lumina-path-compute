from collections import OrderedDict

from lib import generate_edges, find_all_paths, find_degree, find_path_two_vertices, shortest_path_two_vertices
from lib import dist, find_hop_all_leaves, find_closest_vertex_path
graph = {
    1: [2, 5, 6, 7],
    2: [1, 3, 7],
    3: [2, 4, 8],
    4: [3, 5, 9],
    5: [1, 4],
    6: [1, 7, 10],
    7: [1, 2, 6, 11, 12],
    8: [3, 9, 12],
    9: [4, 8],
    10: [6, 11, 14],
    11: [7, 10, 12, 15],
    12: [7, 8, 11, 13, 17],
    13: [12, 18],
    14: [10, 15, 20],
    15: [11, 14, 17, 21],
    17: [12, 15, 22],
    18: [17, 19, 23, 24],
    19: [13, 18, 24],
    20: [14, 21],
    21: [15, 20, 22],
    22: [17, 21, 23],
    23: [18, 22, 24],
    24: [18, 19, 23]
 }

graph = OrderedDict(graph)

# print(generate_edges(graph))
# print(find_path_two_vertices(graph, 1, 6))

# print(shortest_path_two_vertices(graph, 1, 8))
# print(shortest_path_two_vertices(graph, 1, 9))

# print(dist(shortest_path_two_vertices(graph, 8, 9)))
# print(dist(shortest_path_two_vertices(graph, 8, 21)))
# print(dist(shortest_path_two_vertices(graph, 8, 22)))
# print(dist(shortest_path_two_vertices(graph, 8, 24)))


# print(dist(shortest_path_two_vertices(graph, 9, 8)))
# print(dist(shortest_path_two_vertices(graph, 9, 21)))
# print(dist(shortest_path_two_vertices(graph, 9, 22)))
# print(dist(shortest_path_two_vertices(graph, 9, 24)))

start = 1
leaves=[8, 9, 21, 22, 24]

print(shortest_path_two_vertices(graph, 17, 24))

print(find_hop_all_leaves(graph, 1, leaves=[8, 9, 21, 22, 24]))

print('Tie with 8 and 9, at 3 hops')

print(find_hop_all_leaves(graph, 8, leaves=[8, 9, 21, 22, 24]))
print(find_hop_all_leaves(graph, 9, leaves=[8, 9, 21, 22, 24]))


print('Select 8, cause lower')

print('path 1 is [1, 2, 3, 8]')

options = find_all_paths(graph, 1, 8)
option = [i for i in options if len(i) == 4]

print option

for path in option:
    print('Checking length of path {}'.format(path))
    path_distance = 0
    for node in path[1:3]:
        distance = find_hop_all_leaves(graph, node, leaves=[8, 9, 21, 22, 24])
        path_distance = path_distance + distance
        print('For node {} distance to leaves total: {}'.format(node, distance))
    print('path {} had distance {}'.format(path, path_distance))

first_seg = option[1]

print('First segment hop is {}'.format(first_seg))

find_hop_all_leaves(graph, first_seg[-1], leaves)

# Trivial repeat above
first_seg = [1, 7, 12, 8]
second_seg = [8, 9]
third_seg = [9, 8, 12, 17, 22]
fourth_seg = [22, 21]
fifth_seg = [21, 22, 23, 24]

# Find a way to choose the RPs to be {12,8} and {12,22}

spread = [first_seg, second_seg, third_seg, fourth_seg, fifth_seg]

links = {}

for seg in spread:
    end = len(seg)
    for index in xrange(len(seg)):
        # print(index)
        if index != end-1:
            links.update( { seg[index] : seg[index+1] } )        

print('Link efficiency of these segments is {}'.format(len(links), links))
link_efficiency = len(links)

# Find duplication efficiency given RPs (how many packets are passed in the network)
# TODO make recursive, only does 2 layers of RPs, no more no less
# TODO check if RPs are last mile RPs, change how 'used' is marked on an RP to be recursive

leaves = [8, 9, 21, 22, 24]

replication_points = [8,12,22] # Should be a set
rp_list = replication_points[:] # Backup for later
# print(rp_list)
used_replication_points = set()

efficiency1 = 0
for leaf in leaves:
    path = find_closest_vertex_path(graph, leaf, replication_points, verbose=True)
    size = len(path) - 1
    efficiency1 = efficiency1 + size
    print(path[-1])
    used_replication_points.add(path[-1])

print(used_replication_points)
print(efficiency1)

# print(replication_points)
leaves = list(set(replication_points).intersection(used_replication_points))
for i in used_replication_points:
    replication_points.remove(i)
print(list(leaves))
print('remaining replication points {}'.format(replication_points))
used_replication_points = set()

efficiency2 = 0
for leaf in leaves:
    path = find_closest_vertex_path(graph, leaf, replication_points, verbose=False)
    size = len(path) - 1
    efficiency2 = efficiency2 + size
    print(path[-1])
    used_replication_points.add(path[-1])

# print(used_replication_points)
# print(efficiency2)

for i in used_replication_points:
    replication_points.remove(i)

if not replication_points:
    size = find_hop_all_leaves(graph, start, [path[-1]], verbose=False)
    final_size = efficiency1 + efficiency2 + size
    print('Using segments: {}'.format(spread))
    print('The final link efficiency is {}'.format(link_efficiency))
    print('Using replication points: {}'.format(rp_list))
    print(rp_list)
    print('The final duplication efficiency is {}'.format(final_size))
    if final_size == link_efficiency:
        print('Congratulations, you are efficient!!!')

