def generate_edges(graph):
    edges = []
    for nodes in graph:
        for neighbor in graph[nodes]:
            edges.append((nodes,neighbor))
        return edges

def find_path_two_vertices(graph, start, end, path=[]):
    path = path + [start]
    if start == end:
        return path
    if not start in graph:
        return None
    for node in graph[start]:
        if node not in path:
            newpath = find_path_two_vertices(graph, node, end, path)
            if newpath:
                return newpath
    return None

def shortest_path_two_vertices(graph, start, end, path=None):
    if path is None: 
        path = []
    path=path + [start]
    if start == end or end in path:
        return path
    if not start in graph:
        raise ValueError("start not in graph")
    shortest_path = None
    for node in graph[start]:
        if node not in path:
            newpath = shortest_path_two_vertices(graph, node, end, path)
            if newpath:
                if not shortest_path or len(shortest_path) > len(newpath):
                    shortest_path = newpath
    return shortest_path

def find_all_paths(graph, start, end, path=None):
    if path is None: 
        path = []
    path = path + [start]
    if start == end:
        return [path]
    if not start in graph:
        return []
    found_paths = []
    for node in graph[start]:
        if node not in path:
            newpath_find = find_all_paths(graph, node, end, path)
            for newpaths in newpath_find:
                found_paths.append(newpaths)
    return found_paths

def find_degree(graph, node):
    degree = 0
    t = []
    for neighbor in graph[node]:
        t.append(neighbor)
        degree = degree + 1
    return degree

def dist(path):
    return len(path) - 1

def find_hop_all_leaves(graph, start, leaves=None, verbose=False):
    total = 0
    if leaves is None:
        leaves = []
    if start in leaves:
        leaves.remove(start)
    for leaf in leaves:
        path = shortest_path_two_vertices(graph, start, leaf)
        size = dist(path)
        if verbose:
            print('length of {} to {} is {}'.format(start, leaf, size))
            print(path)
        total = total + size
    return total

def find_closest_vertex_path(graph, start, verticies, verbose=False):
    shortest_path = None
    if verticies is None:
        leaves = []
    if start in verticies:
        return [start]
    for vertex in verticies:
        path = shortest_path_two_vertices(graph, start, vertex)
        size = dist(path)
        if verbose:
            print('length of {} to {} is {}'.format(start, vertex, size))
            print(path)
        if not shortest_path or len(shortest_path) > len(path):
            shortest_path = path
    return shortest_path